import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'groweriq-signup';

  constructor(private http: HttpClient) { }

  visibleColumns = ['name', 'email', 'type'];


  userData$ = this.http.get('http://5ccc5842f47db800140110d0.mockapi.io/users');


}
